import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';

class Utilities {
  /// This is the default inBase style toast notification
  static Future<void> showToast(String message) async {
    await Fluttertoast.showToast(
        msg: message,
        backgroundColor: HexColor("#1e517b"),
        textColor: Colors.white,
        gravity: ToastGravity.TOP,
        fontSize: 14);
  }

  /// This will write in logs
  static void appLog(String message) {
    log("================================= WEBINIFY Logs : START =================================");
    log(message);
    log("================================= WEBINIFY Logs : END   =================================");
  }
}
