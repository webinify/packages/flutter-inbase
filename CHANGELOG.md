## 0.0.7

* Adding HexColor
* Showtoast restyled
* 

## 0.0.6

* Adding package info plus package

## 0.0.3

* Some fixes
* Adding some comments
* Refactoring

## 0.0.2

* Project initiated
