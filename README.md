<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

Are you looking for an online free service to handle errors, crash reports and so on ? You are at the right place.
Cloud based inbase service to track your apps health with Webinify advantages.

This is the core package of inbase for Flutter apps. You must install this in order to use any plugins such as inbase_crashlitycs !

## Features

This package enables your app to be registered and works with all extended plugins.

## Getting started

All that you'll need to initiate the package in the main function and you are set to use any inbase extended packages like:
-   inbase_crashlitycs

More packages are on the way...

## Usage

```dart
 main() async {
  
  
  /// Here we initiate the inBase package
  await InBase.initializeApp(
      InBaseConfig(
          projectId: YOUR_RPOJECT_ID, 
          apiKey: YOUR_PROJECT_API_KEY, 
          showToastOnError: true
      )
  );
  
}
```

## Additional information

This package is under development and can't be used at this stage. 