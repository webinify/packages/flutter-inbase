import 'dart:developer';

import 'package:encrypted_shared_preferences/encrypted_shared_preferences.dart';
import 'Utilities.dart';
import 'package:package_info_plus/package_info_plus.dart';

/// This is the configuration object of InBase
class InBaseConfig {
  InBaseConfig(
      {required this.projectId,
      required this.apiKey,
      this.showToastOnError,
      this.appVersion,
      this.appPackageName});

  /// This will initiate package info details for the app
  initPackageInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    appPackageName ??= packageInfo.packageName;
    appVersion ??= packageInfo.version;

    Utilities.appLog("App PackageName: $appPackageName"
        "App version: $appVersion");
  }

  /// This is the package name
  String? appPackageName;

  /// This is the app version
  /// You must set this
  String? appVersion;

  /// Your project ID
  /// Generated from inbase.webinify.com
  int projectId;

  /// Your project API key
  /// Generated from inbase.webinify.com
  String apiKey;

  /// This will enable or disable the FlutterToastNotifications
  /// By default it set to true
  bool? showToastOnError = true;

  EncryptedSharedPreferences encryptedSharedPreferences =
      EncryptedSharedPreferences();

  /// Definition of all sharedPreferenceKeys
  static const String sharedPreferenceProjectId = "inbase-project-id";
  static const String sharedPreferenceApiKey = "inbase-api-key";
  static const String sharedPreferenceShowToastKey =
      "inbase-app-config-show-toast";

  /// This function will save the app credentials in encrypted SharedPreferences
  Future<void> save() async {
    try {
      await encryptedSharedPreferences.setString(
          sharedPreferenceProjectId, projectId.toString());
      await encryptedSharedPreferences.setString(
          sharedPreferenceApiKey, apiKey);
      await encryptedSharedPreferences.setString(
          sharedPreferenceShowToastKey, showToastOnError.toString());
    } catch (e) {
      if (showToastOnError == true) {
        await Utilities.showToast(e.toString());
      }
    }
  }

  /// This will return the saved config
  static Future<InBaseConfig> getConfig() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    EncryptedSharedPreferences encryptedSharedPreferences =
        EncryptedSharedPreferences();

    InBaseConfig inBaseConfig = InBaseConfig(
        projectId: int.parse(await encryptedSharedPreferences
            .getString(sharedPreferenceProjectId)),
        apiKey:
            await encryptedSharedPreferences.getString(sharedPreferenceApiKey),
        showToastOnError: await encryptedSharedPreferences
                    .getString(sharedPreferenceShowToastKey) ==
                "true"
            ? true
            : false,
        appPackageName: packageInfo.packageName,
        appVersion: packageInfo.version);

    return inBaseConfig;
  }
}
