library inbase;

import 'package:flutter/foundation.dart';
import 'package:inbase/InBaseConfig.dart';
import 'package:inbase/Utilities.dart';

/// Core class of InBase package
class InBase {

  /// This function will initiate the package
  /// And will be saved to device on load
  static initializeApp(InBaseConfig inBaseConfig) async {
    await inBaseConfig.initPackageInfo();

    if (kDebugMode) {
      Utilities.appLog("InBase initiated !");
    }

    await inBaseConfig.save();
  }
}
